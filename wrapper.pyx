#cython: language_level=3
# above: tells cython to use python3

# import numpy as pure python and C counterpart
import numpy as np 
cimport numpy as np
from libc.stdlib cimport free

# define the standard C prototypes
cdef extern from "libexample.h":
    double *hello(const char *name, int n, double d, double *vec)

# python translation of each py_something function
# input arrays must be casted as shown in the vec parameter
def py_hello(name, n, d, np.ndarray[double, ndim=1, mode="c"] vec):
    # prepare C pointer to receive output
    cdef double *out
    # call the C function passing address of vec
    out = hello(name, n, d, &vec[0])
    
    # translate the output pointers into a numpy array
    parray = np.zeros(n)
    for i in range(n):
        parray[i] = out[i]
    
    # free the out pointer
    free(out)
    # return the numpy array
    return(parray)

