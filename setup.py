from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
import numpy as np

the_extension = Extension(
    name="mylib",
    sources=["wrapper.pyx"],
    define_macros = [('NPY_NO_DEPRECATED_API', 'NPY_1_7_API_VERSION')],
    extra_compile_args = ["-fPIC"], 
    libraries=["example"],
    library_dirs=["CodeC"],
    include_dirs=["CodeC", np.get_include()]
) 
    
setup(
    name="mylib",
    ext_modules=cythonize([the_extension])
)
